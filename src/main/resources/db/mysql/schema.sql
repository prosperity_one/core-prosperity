CREATE TABLE IF NOT EXISTS vets (
  id INT(4) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  first_name VARCHAR(30),
  last_name VARCHAR(30),
  INDEX(last_name)
) engine=InnoDB;

CREATE TABLE IF NOT EXISTS specialties (
  id INT(4) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(80),
  INDEX(name)
) engine=InnoDB;

CREATE TABLE IF NOT EXISTS vet_specialties (
  vet_id INT(4) UNSIGNED NOT NULL,
  specialty_id INT(4) UNSIGNED NOT NULL,
  FOREIGN KEY (vet_id) REFERENCES vets(id),
  FOREIGN KEY (specialty_id) REFERENCES specialties(id),
  UNIQUE (vet_id,specialty_id)
) engine=InnoDB;

CREATE TABLE IF NOT EXISTS product_types (
  id INT(4) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(80),
  INDEX(name)
) engine=InnoDB;

CREATE TABLE IF NOT EXISTS products (
  id int(50) unsigned NOT NULL AUTO_INCREMENT,
  bar_code varchar(50) DEFAULT NULL,
  name varchar(50) DEFAULT NULL,
  description varchar(30) DEFAULT NULL,
  lot_id int(50) unsigned DEFAULT NULL,
  type_id int(4) unsigned NOT NULL,
  PRIMARY KEY (id),
  KEY type_id (type_id),
  KEY name (name),
  KEY products_FK (lot_id),
  CONSTRAINT products_ibfk_1 FOREIGN KEY (type_id) REFERENCES product_types (id)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- CREATE INDEX products_name ON products (name);

CREATE TABLE IF NOT EXISTS cliente (
  id INT(4) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  first_name VARCHAR(30),
  last_name VARCHAR(30),
  address VARCHAR(255),
  city VARCHAR(80),
  telephone VARCHAR(20),
  INDEX(last_name)
) engine=InnoDB;

CREATE TABLE IF NOT EXISTS lot (
  id int(50) unsigned NOT NULL AUTO_INCREMENT,
  expire_date date DEFAULT NULL,
  code varchar(100) NOT NULL,
  product_id int(50) unsigned NOT NULL,
  quantity int(10) unsigned DEFAULT NULL,
  price double DEFAULT NULL,
  price_paid double DEFAULT NULL,
  suplier varchar(50) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY lot_FK (product_id),
  CONSTRAINT lot_FK FOREIGN KEY (product_id) REFERENCES products (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS sale (
  lot_id int(50) unsigned NOT NULL,
  id int(50) unsigned NOT NULL AUTO_INCREMENT,
  price_paid double(10,2) NOT NULL,
  client_id int(50) unsigned DEFAULT NULL,
  quantity int(10) unsigned NOT NULL,
  PRIMARY KEY (id),
  KEY sale_FK (lot_id),
  KEY sale_FK_1 (client_id),
  CONSTRAINT sale_FK FOREIGN KEY (lot_id) REFERENCES lot (id),
  CONSTRAINT sale_FK_1 FOREIGN KEY (client_id) REFERENCES cliente (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS pets (
  id INT(4) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(30),
  birth_date DATE,
  type_id INT(4) UNSIGNED NOT NULL,
  owner_id INT(4) UNSIGNED NOT NULL,
  INDEX(name),
  FOREIGN KEY (owner_id) REFERENCES cliente(id),
  FOREIGN KEY (type_id) REFERENCES PRODUCT_TYPES(id)
) engine=InnoDB;

CREATE TABLE IF NOT EXISTS visits (
  id INT(4) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  pet_id INT(4) UNSIGNED NOT NULL,
  visit_date DATE,
  description VARCHAR(255),
  FOREIGN KEY (pet_id) REFERENCES pets(id)
) engine=InnoDB;
