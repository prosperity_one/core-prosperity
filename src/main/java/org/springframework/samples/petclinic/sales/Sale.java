/*
 * Copyright 2012-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.sales;

import lombok.Data;
import org.springframework.samples.petclinic.client.Client;
import org.springframework.samples.petclinic.model.BaseEntity;
import org.springframework.samples.petclinic.products.Lot;
import org.springframework.samples.petclinic.products.Product;
import org.springframework.samples.petclinic.products.ProductType;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;

/**
 * Simple JavaBean domain object representing a veterinarian.
 *
 * @author Ken Krebs
 * @author Juergen Hoeller
 * @author Sam Brannen
 * @author Arjen Poutsma
 */
@Entity
@Data
@Table(name = "sale")
public class Sale extends BaseEntity {

	//
	// `lot_id` int(50) unsigned NOT NULL,
	// `id` int(50) unsigned NOT NULL AUTO_INCREMENT,
	// `price_paid` double(10,2) NOT NULL,
	// `client_id` int(50) unsigned DEFAULT NULL,
	// `quantity` int(10) unsigned NOT NULL,

	@ManyToOne
	@JoinColumn(name = "lot_id")
	private Lot product;

	@ManyToOne
	@JoinColumn(name = "client_id")
	private Client client;

	@Column(name = "price_paid")
	private BigDecimal price_paid;

	@Column(name = "quantity")
	private Integer quantity;

}
