package org.springframework.samples.petclinic.client;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Getter
@Service
@AllArgsConstructor
@NoArgsConstructor
public class ClientPages {

	@Value("${forms.pages.clients.list}")
	private String clientsListPage;

	@Value("${forms.pages.clients.search}")
	private String clientsSearchPage;

	@Value("${forms.pages.clients.details}")
	private String clientsDetailsPage;

	@Value("${forms.pages.clients.create-update}")
	private String createOrUpdateClientsPage;

}
