/*
 * Copyright 2012-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.client;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.samples.petclinic.visit.VisitRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Map;

/**
 * @author Juergen Hoeller
 * @author Ken Krebs
 * @author Arjen Poutsma
 * @author Michael Isvy
 */
@Controller
@AllArgsConstructor
class ClientController {

	private final ClientPages clientPages;

	private final OwnerRepository owners;

	private final VisitRepository visits;

	@InitBinder
	public void setAllowedFields(WebDataBinder dataBinder) {
		dataBinder.setDisallowedFields("id");
	}

	@GetMapping("/clientes/novo")
	public String initCreationForm(Map<String, Object> model) {
		Client client = new Client();
		model.put("client", client);
		return clientPages.getCreateOrUpdateClientsPage();
	}

	@PostMapping("/clientes/novo")
	public String processCreationForm(@Valid Client client, BindingResult result) {
		if (result.hasErrors()) {
			return clientPages.getCreateOrUpdateClientsPage();
		}
		else {
			this.owners.save(client);
			return "redirect:/clientes/" + client.getId();
		}
	}

	@GetMapping("/clientes/procurar")
	public String initFindForm(Map<String, Object> model) {
		model.put("client", new Client());
		return clientPages.getClientsSearchPage();
	}

	@GetMapping("/clientes")
	public String processFindForm(Client client, BindingResult result, Map<String, Object> model) {

		// allow parameterless GET request for /clientes to return all records
		if (client.getLastName() == null) {
			client.setLastName(""); // empty string signifies broadest possible search
		}

		// find owners by last name
		Collection<Client> results = this.owners.findByLastName(client.getLastName());
		if (results.isEmpty()) {
			// no client found
			result.rejectValue("lastName", "notFound", "not found");
			return clientPages.getClientsSearchPage();
		}
		else if (results.size() == 1) {
			// 1 client found
			client = results.iterator().next();
			return "redirect:/clientes/" + client.getId();
		}
		else {
			// multiple clients found
			model.put("selections", results);
			return clientPages.getClientsListPage();
		}
	}

	@GetMapping("/clientes/{ownerId}/edit")
	public String initUpdateOwnerForm(@PathVariable("ownerId") int ownerId, Model model) {
		Client client = this.owners.findById(ownerId);
		model.addAttribute(client);
		return clientPages.getCreateOrUpdateClientsPage();
	}

	@PostMapping("/clientes/{ownerId}/edit")
	public String processUpdateOwnerForm(@Valid Client client, BindingResult result,
			@PathVariable("ownerId") int ownerId) {
		if (result.hasErrors()) {
			return clientPages.getCreateOrUpdateClientsPage();
		}
		else {
			client.setId(ownerId);
			this.owners.save(client);
			return "redirect:/clientes/{ownerId}";
		}
	}

	/**
	 * Custom handler for displaying an owner.
	 * @param ownerId the ID of the owner to display
	 * @return a ModelMap with the model attributes for the view
	 */
	@GetMapping("/clientes/{ownerId}")
	public ModelAndView showOwner(@PathVariable("ownerId") int ownerId) {
		ModelAndView mav = new ModelAndView(clientPages.getClientsDetailsPage());
		Client client = this.owners.findById(ownerId);
		for (Pet pet : client.getPets()) {
			pet.setVisitsInternal(visits.findByPetId(pet.getId()));
		}
		mav.addObject(client);
		return mav;
	}

}
