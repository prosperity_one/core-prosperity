package org.springframework.samples.petclinic.products;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Getter
@Service
@AllArgsConstructor
@NoArgsConstructor
public class ProductsPages {

	@Value("${forms.pages.products.list}")
	private String productsListPage;

	@Value("${forms.pages.products.search}")
	private String productsSearchPage;

	@Value("${forms.pages.products.details}")
	private String productsDetailsPage;

	@Value("${forms.pages.products.create-update}")
	private String createUpdateProductsPage;

}
