/*
 * Copyright 2012-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.products;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;

/**
 * @author Juergen Hoeller
 * @author Mark Fisher
 * @author Ken Krebs
 * @author Arjen Poutsma
 */
@Controller
@AllArgsConstructor
public class ProductsController {

	private final ProductRepository productRepository;

	private final LotRepository lotRepository;

	private final ProductTypeRepository productTypeRepository;

	private final ProductsPages productsPages;

	@ModelAttribute("product_types")
	public Collection<ProductType> populateProductTypes() {
		return this.productTypeRepository.findAll();
	}

	@GetMapping("/produtos")
	public String showProductList(Product product, BindingResult result, Map<String, Object> model) {

		if (product.getName() == null) {
			product.setName("");
		}

		// find product by name
		Collection<Product> results = this.productRepository.findByName(product.getName());
		if (results.isEmpty()) {
			// no product found
			result.rejectValue("name", "notFound", "not found");
			return productsPages.getProductsSearchPage();
		}
		else {
			results.forEach(product1 -> calculateProductsQuantity(product1));

			model.put("selections", results);
			return productsPages.getProductsListPage();
		}
	}

	private void calculateProductsQuantity(Product product) {
		product.setProductsQUantity(0);
		product.getLots().forEach(lot -> {
			product.setProductsQUantity(product.getProductsQUantity() + lot.getQuantity());
		});
	}

	@GetMapping("/produtos/novo")
	public String initCreationForm(Map<String, Object> model) {
		model.put("product", new Product());
		model.put("lot", new Lot());
		return productsPages.getCreateUpdateProductsPage();
	}

	@PostMapping("/produtos/novo")
	public String processCreationForm(@Valid Product product, @Valid Lot lot, BindingResult result) {
		if (result.hasErrors()) {
			return productsPages.getCreateUpdateProductsPage();
		}
		else {
			this.productRepository.save(product);
			lot.setProduct(product);
			this.lotRepository.save(lot);
			return "redirect:/produtos/";
		}
	}

	@GetMapping("/produtos/procurar")
	public String initFindForm(Map<String, Object> model) {
		model.put("product", new Product());
		return productsPages.getProductsSearchPage();
	}

}
