package br.com.presperity.config;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.samples.petclinic.client.ClientPages;

@TestConfiguration
public class ConfigModule {

	@Bean
	public ClientPages clientPages() {
		return new ClientPages();
	}

}
