/*
 * Copyright 2012-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.samples.petclinic.client;

import org.assertj.core.util.Lists;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.samples.petclinic.products.ProductType;
import org.springframework.samples.petclinic.visit.Visit;
import org.springframework.samples.petclinic.visit.VisitRepository;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for {@link ClientController}
 *
 * @author Lucas Schunk
 */
@WebMvcTest(ClientController.class)
class ClientControllerTests {

	private static final int TEST_OWNER_ID = 1;

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private OwnerRepository owners;

	@MockBean
	private ClientPages clientPages;

	@MockBean
	private VisitRepository visits;

	private Client george;

	@BeforeEach
	void setup() {
		george = new Client();
		george.setId(TEST_OWNER_ID);
		george.setFirstName("George");
		george.setLastName("Franklin");
		george.setAddress("110 W. Liberty St.");
		george.setCity("Madison");
		george.setTelephone("6085551023");
		Pet max = new Pet();
		ProductType dog = new ProductType();
		dog.setName("dog");
		max.setId(1);
		max.setType(dog);
		max.setName("Max");
		max.setBirthDate(LocalDate.now());
		george.setPetsInternal(Collections.singleton(max));
		given(this.owners.findById(TEST_OWNER_ID)).willReturn(george);
		Visit visit = new Visit();
		visit.setDate(LocalDate.now());
		given(this.visits.findByPetId(max.getId())).willReturn(Collections.singletonList(visit));
		mockPages();
	}

	private void mockPages() {
		given(this.clientPages.getCreateOrUpdateClientsPage()).willReturn("clients/createOrUpdateClientsForm");
		given(this.clientPages.getClientsDetailsPage()).willReturn("clients/clientsDetails");
		given(this.clientPages.getClientsListPage()).willReturn("clients/clientsList");
		given(this.clientPages.getClientsSearchPage()).willReturn("clients/clientsSearch");
	}

	@Test
	void testInitCreationForm() throws Exception {
		mockMvc.perform(get("/clientes/novo")).andExpect(status().isOk()).andExpect(model().attributeExists("client"))
				.andExpect(view().name("clients/createOrUpdateClientsForm"));
	}

	@Test
	void testProcessCreationFormSuccess() throws Exception {
		mockMvc.perform(post("/clientes/novo").param("firstName", "Joe").param("lastName", "Bloggs")
				.param("address", "123 Caramel Street").param("city", "London").param("telephone", "01316761638"))
				.andExpect(status().is3xxRedirection());
	}

	@Test
	void testProcessCreationFormHasErrors() throws Exception {
		mockMvc.perform(
				post("/clientes/novo").param("firstName", "Joe").param("lastName", "Bloggs").param("city", "London"))
				.andExpect(status().isOk()).andExpect(model().attributeHasErrors("client"))
				.andExpect(model().attributeHasFieldErrors("client", "address"))
				.andExpect(model().attributeHasFieldErrors("client", "telephone"))
				.andExpect(view().name("clients/createOrUpdateClientsForm"));
	}

	@Test
	void testInitFindForm() throws Exception {
		mockMvc.perform(get("/clientes/procurar")).andExpect(status().isOk())
				.andExpect(model().attributeExists("client")).andExpect(view().name("clients/clientsSearch"));
	}

	@Test
	void testProcessFindFormSuccess() throws Exception {
		given(this.owners.findByLastName("")).willReturn(Lists.newArrayList(george, new Client()));
		mockMvc.perform(get("/clientes")).andExpect(status().isOk()).andExpect(view().name("clients/clientsList"));
	}

	@Test
	void testProcessFindFormByLastName() throws Exception {
		given(this.owners.findByLastName(george.getLastName())).willReturn(Lists.newArrayList(george));
		mockMvc.perform(get("/clientes").param("lastName", "Franklin")).andExpect(status().is3xxRedirection())
				.andExpect(view().name("redirect:/clientes/" + TEST_OWNER_ID));
	}

	@Test
	void testProcessFindFormNoOwnersFound() throws Exception {
		mockMvc.perform(get("/clientes").param("lastName", "Unknown Surname")).andExpect(status().isOk())
				.andExpect(model().attributeHasFieldErrors("client", "lastName"))
				.andExpect(model().attributeHasFieldErrorCode("client", "lastName", "notFound"))
				.andExpect(view().name("clients/clientsSearch"));
	}

	@Test
	void testInitUpdateOwnerForm() throws Exception {
		mockMvc.perform(get("/clientes/{ownerId}/edit", TEST_OWNER_ID)).andExpect(status().isOk())
				.andExpect(model().attributeExists("client"))
				.andExpect(model().attribute("client", hasProperty("lastName", is("Franklin"))))
				.andExpect(model().attribute("client", hasProperty("firstName", is("George"))))
				.andExpect(model().attribute("client", hasProperty("address", is("110 W. Liberty St."))))
				.andExpect(model().attribute("client", hasProperty("city", is("Madison"))))
				.andExpect(model().attribute("client", hasProperty("telephone", is("6085551023"))))
				.andExpect(view().name("clients/createOrUpdateClientsForm"));
	}

	@Test
	void testProcessUpdateOwnerFormSuccess() throws Exception {
		mockMvc.perform(post("/clientes/{ownerId}/edit", TEST_OWNER_ID).param("firstName", "Joe")
				.param("lastName", "Bloggs").param("address", "123 Caramel Street").param("city", "London")
				.param("telephone", "01616291589")).andExpect(status().is3xxRedirection())
				.andExpect(view().name("redirect:/clientes/{ownerId}"));
	}

	@Test
	void testProcessUpdateOwnerFormHasErrors() throws Exception {
		mockMvc.perform(post("/clientes/{ownerId}/edit", TEST_OWNER_ID).param("firstName", "Joe")
				.param("lastName", "Bloggs").param("city", "London")).andExpect(status().isOk())
				.andExpect(model().attributeHasErrors("client"))
				.andExpect(model().attributeHasFieldErrors("client", "address"))
				.andExpect(model().attributeHasFieldErrors("client", "telephone"))
				.andExpect(view().name("clients/createOrUpdateClientsForm"));
	}

	@Test
	void testShowOwner() throws Exception {
		mockMvc.perform(get("/clientes/{ownerId}", TEST_OWNER_ID)).andExpect(status().isOk())
				.andExpect(model().attribute("client", hasProperty("lastName", is("Franklin"))))
				.andExpect(model().attribute("client", hasProperty("firstName", is("George"))))
				.andExpect(model().attribute("client", hasProperty("address", is("110 W. Liberty St."))))
				.andExpect(model().attribute("client", hasProperty("city", is("Madison"))))
				.andExpect(model().attribute("client", hasProperty("telephone", is("6085551023"))))
				.andExpect(model().attribute("client", hasProperty("pets", not(empty()))))
				.andExpect(model().attribute("client", hasProperty("pets", new BaseMatcher<List<Pet>>() {

					@Override
					public boolean matches(Object item) {
						@SuppressWarnings("unchecked")
						List<Pet> pets = (List<Pet>) item;
						Pet pet = pets.get(0);
						if (pet.getVisits().isEmpty()) {
							return false;
						}
						return true;
					}

					@Override
					public void describeTo(Description description) {
						description.appendText("Max did not have any visits");
					}
				}))).andExpect(view().name("clients/clientsDetails"));
	}

}
