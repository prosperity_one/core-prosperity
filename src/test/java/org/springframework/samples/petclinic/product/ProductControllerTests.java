package org.springframework.samples.petclinic.product;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.samples.petclinic.products.ProductType;
import org.springframework.samples.petclinic.products.*;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for {@link ProductsController}
 *
 * @author Lucas Schunk
 */
@WebMvcTest(ProductsController.class)
class ProductControllerTests {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ProductRepository productRepository;

	@MockBean
	private LotRepository lotRepository;

	@MockBean
	private ProductTypeRepository productTypeRepository;

	@MockBean
	private ProductsPages productsPages;

	private Product test;

	private ProductType productType;

	@BeforeEach
	void setup() {
		productType = new ProductType();
		productType.setId(1);
		productType.setName("Tipo 1");
		ProductType type2 = new ProductType();
		type2.setId(2);
		type2.setName("Tipo 2");
		List<ProductType> productTypes = new ArrayList<>() {
			{
				add(productType);
				add(type2);
			}
		};
		given(this.productTypeRepository.findAll()).willReturn(productTypes);

		mockPages();
	}

	private void mockPages() {
		given(this.productsPages.getCreateUpdateProductsPage()).willReturn("products/createOrUpdateProductsForm");
		given(this.productsPages.getProductsDetailsPage()).willReturn("products/productsDetails");
		given(this.productsPages.getProductsListPage()).willReturn("products/productsList");
		given(this.productsPages.getProductsSearchPage()).willReturn("products/productsSearch");
	}

	@Test
	void testInitCreationForm() throws Exception {
		mockMvc.perform(get("/produtos/novo")).andExpect(status().isOk()).andExpect(model().attributeExists("product"))
				.andExpect(view().name("products/createOrUpdateProductsForm"));
	}

	@Test
	void testProcessCreationFormSuccess() throws Exception {
		mockMvc.perform(post("/produtos/novo").param("product.barCode", "123123123123").param("product.name", "Perfume")
				.param("product.description", "Perfume masculino kaiak").param("lot.code", "123123")
				.param("lot.suplier", "natura").param("lot.expireDate", "2020-06-07").param("lot.price", "15.6"))
				.andExpect(status().is3xxRedirection());
	}

	@Test
	void testProcessCreationFormHasErrors() throws Exception {
		mockMvc.perform(post("/produtos/novo").param("product.barCode", "123123123123").param("product.name", "Perfume")
				.param("product.description", "Perfume masculino kaiak").param("lot.code", "123123")
				.param("suplier", "natura").param("expireDate", "2014-24-24").param("price", "ASDA")
				.param("quantity", "1as")).andExpect(status().isOk()).andExpect(model().attributeHasErrors("lot"))
				.andExpect(model().attributeHasFieldErrors("lot", "expireDate"))
				.andExpect(model().attributeHasFieldErrors("lot", "price"))
				.andExpect(model().attributeHasFieldErrors("lot", "quantity"))
				.andExpect(view().name("products/createOrUpdateProductsForm"));
	}

	@Test
	void testInitFindForm() throws Exception {
		mockMvc.perform(get("/produtos/procurar")).andExpect(status().isOk())
				.andExpect(model().attributeExists("product")).andExpect(view().name("products/productsSearch"));
	}

	// @Test
	// void testProcessFindFormSuccess() throws Exception {
	// given(this.owners.findByLastName("")).willReturn(Lists.newArrayList(george, new
	// Client()));
	// mockMvc.perform(get("/produtos")).andExpect(status().isOk()).andExpect(view().name("products/productsList"));
	// }
	//
	// @Test
	// void testProcessFindFormByLastName() throws Exception {
	// given(this.owners.findByLastName(george.getLastName())).willReturn(Lists.newArrayList(george));
	// mockMvc.perform(get("/produtos").param("lastName",
	// "Franklin")).andExpect(status().is3xxRedirection())
	// .andExpect(view().name("redirect:/produtos/" + TEST_OWNER_ID));
	// }
	//
	// @Test
	// void testProcessFindFormNoOwnersFound() throws Exception {
	// mockMvc.perform(get("/produtos").param("lastName", "Unknown
	// Surname")).andExpect(status().isOk())
	// .andExpect(model().attributeHasFieldErrors("client", "lastName"))
	// .andExpect(model().attributeHasFieldErrorCode("client", "lastName", "notFound"))
	// .andExpect(view().name("products/productsSearch"));
	// }
	//
	// @Test
	// void testInitUpdateOwnerForm() throws Exception {
	// mockMvc.perform(get("/produtos/{ownerId}/edit",
	// TEST_OWNER_ID)).andExpect(status().isOk())
	// .andExpect(model().attributeExists("client"))
	// .andExpect(model().attribute("client", hasProperty("lastName", is("Franklin"))))
	// .andExpect(model().attribute("client", hasProperty("firstName", is("George"))))
	// .andExpect(model().attribute("client", hasProperty("address", is("110 W. Liberty
	// St."))))
	// .andExpect(model().attribute("client", hasProperty("city", is("Madison"))))
	// .andExpect(model().attribute("client", hasProperty("telephone", is("6085551023"))))
	// .andExpect(view().name("products/createOrUpdateproductsForm"));
	// }
	//
	// @Test
	// void testProcessUpdateOwnerFormSuccess() throws Exception {
	// mockMvc.perform(post("/produtos/{ownerId}/edit", TEST_OWNER_ID).param("firstName",
	// "Joe")
	// .param("lastName", "Bloggs").param("address", "123 Caramel Street").param("city",
	// "London")
	// .param("telephone", "01616291589")).andExpect(status().is3xxRedirection())
	// .andExpect(view().name("redirect:/produtos/{ownerId}"));
	// }
	//
	// @Test
	// void testProcessUpdateOwnerFormHasErrors() throws Exception {
	// mockMvc.perform(post("/produtos/{ownerId}/edit", TEST_OWNER_ID).param("firstName",
	// "Joe")
	// .param("lastName", "Bloggs").param("city", "London")).andExpect(status().isOk())
	// .andExpect(model().attributeHasErrors("client"))
	// .andExpect(model().attributeHasFieldErrors("client", "address"))
	// .andExpect(model().attributeHasFieldErrors("client", "telephone"))
	// .andExpect(view().name("products/createOrUpdateproductsForm"));
	// }
	//
	// @Test
	// void testShowOwner() throws Exception {
	// mockMvc.perform(get("/produtos/{ownerId}",
	// TEST_OWNER_ID)).andExpect(status().isOk())
	// .andExpect(model().attribute("client", hasProperty("lastName", is("Franklin"))))
	// .andExpect(model().attribute("client", hasProperty("firstName", is("George"))))
	// .andExpect(model().attribute("client", hasProperty("address", is("110 W. Liberty
	// St."))))
	// .andExpect(model().attribute("client", hasProperty("city", is("Madison"))))
	// .andExpect(model().attribute("client", hasProperty("telephone", is("6085551023"))))
	// .andExpect(model().attribute("client", hasProperty("pets", not(empty()))))
	// .andExpect(model().attribute("client", hasProperty("pets", new
	// BaseMatcher<List<Pet>>() {
	//
	// @Override
	// public boolean matches(Object item) {
	// @SuppressWarnings("unchecked")
	// List<Pet> pets = (List<Pet>) item;
	// Pet pet = pets.get(0);
	// if (pet.getVisits().isEmpty()) {
	// return false;
	// }
	// return true;
	// }
	//
	// @Override
	// public void describeTo(Description description) {
	// description.appendText("Max did not have any visits");
	// }
	// }))).andExpect(view().name("products/productsDetails"));
	// }

}
