/*
 * Copyright 2012-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.samples.petclinic.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.samples.petclinic.client.*;
import org.springframework.samples.petclinic.products.ProductType;
import org.springframework.samples.petclinic.vet.Vet;
import org.springframework.samples.petclinic.vet.VetRepository;
import org.springframework.samples.petclinic.visit.Visit;
import org.springframework.samples.petclinic.visit.VisitRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration test of the Service and the Repository layer.
 * <p>
 * ClinicServiceSpringDataJpaTests subclasses benefit from the following services provided
 * by the Spring TestContext Framework:
 * </p>
 * <ul>
 * <li><strong>Spring IoC container caching</strong> which spares us unnecessary set up
 * time between test execution.</li>
 * <li><strong>Dependency Injection</strong> of test fixture instances, meaning that we
 * don't need to perform application context lookups. See the use of
 * {@link Autowired @Autowired} on the <code>{@link
 * ClinicServiceTests#clinicService clinicService}</code> instance variable, which uses
 * autowiring <em>by type</em>.
 * <li><strong>Transaction management</strong>, meaning each test method is executed in
 * its own transaction, which is automatically rolled back by default. Thus, even if tests
 * insert or otherwise change database state, there is no need for a teardown or cleanup
 * script.
 * <li>An {@link org.springframework.context.ApplicationContext ApplicationContext} is
 * also inherited and can be used for explicit bean lookup if necessary.</li>
 * </ul>
 *
 * @author Ken Krebs
 * @author Rod Johnson
 * @author Juergen Hoeller
 * @author Sam Brannen
 * @author Michael Isvy
 * @author Dave Syer
 */
@DataJpaTest(includeFilters = @ComponentScan.Filter(Service.class))
class ClinicServiceTests {

	@Autowired
	protected OwnerRepository owners;

	@Autowired
	protected PetRepository pets;

	@Autowired
	protected VisitRepository visits;

	@Autowired
	protected VetRepository vets;

	@Test
	void shouldFindOwnersByLastName() {
		Collection<Client> clients = this.owners.findByLastName("Davis");
		assertThat(clients).hasSize(2);

		clients = this.owners.findByLastName("Daviss");
		assertThat(clients).isEmpty();
	}

	@Test
	void shouldFindSingleOwnerWithPet() {
		Client clientes = this.owners.findById(1);
		assertThat(clientes.getLastName()).startsWith("Franklin");
		assertThat(clientes.getPets()).hasSize(1);
		assertThat(clientes.getPets().get(0).getType()).isNotNull();
		assertThat(clientes.getPets().get(0).getType().getName()).isEqualTo("Perfume");
	}

	@Test
	@Transactional
	void shouldInsertOwner() {
		Collection<Client> clients = this.owners.findByLastName("Schultz");
		int found = clients.size();

		Client client = new Client();
		client.setFirstName("Sam");
		client.setLastName("Schultz");
		client.setAddress("4, Evans Street");
		client.setCity("Wollongong");
		client.setTelephone("4444444444");
		this.owners.save(client);
		assertThat(client.getId().longValue()).isNotEqualTo(0);

		clients = this.owners.findByLastName("Schultz");
		assertThat(clients.size()).isEqualTo(found + 1);
	}

	@Test
	@Transactional
	void shouldUpdateOwner() {
		Client client = this.owners.findById(1);
		String oldLastName = client.getLastName();
		String newLastName = oldLastName + "X";

		client.setLastName(newLastName);
		this.owners.save(client);

		// retrieving new name from database
		client = this.owners.findById(1);
		assertThat(client.getLastName()).isEqualTo(newLastName);
	}

	@Test
	void shouldFindPetWithCorrectId() {
		Pet pet7 = this.pets.findById(7);
		assertThat(pet7.getName()).startsWith("Samantha");
		assertThat(pet7.getClient().getFirstName()).isEqualTo("Jean");

	}

	@Test
	void shouldFindAllPetTypes() {
		Collection<ProductType> petTypes = this.pets.findPetTypes();

		ProductType petType1 = EntityUtils.getById(petTypes, ProductType.class, 1);
		assertThat(petType1.getName()).isEqualTo("Perfume");
		ProductType petType4 = EntityUtils.getById(petTypes, ProductType.class, 4);
		assertThat(petType4.getName()).isEqualTo("Loção Hidratante");
	}

	@Test
	@Transactional
	void shouldInsertPetIntoDatabaseAndGenerateId() {
		Client client6 = this.owners.findById(6);
		int found = client6.getPets().size();

		Pet pet = new Pet();
		pet.setName("bowser");
		Collection<ProductType> types = this.pets.findPetTypes();
		pet.setType(EntityUtils.getById(types, ProductType.class, 2));
		pet.setBirthDate(LocalDate.now());
		client6.addPet(pet);
		assertThat(client6.getPets().size()).isEqualTo(found + 1);

		this.pets.save(pet);
		this.owners.save(client6);

		client6 = this.owners.findById(6);
		assertThat(client6.getPets().size()).isEqualTo(found + 1);
		// checks that id has been generated
		assertThat(pet.getId()).isNotNull();
	}

	@Test
	@Transactional
	void shouldUpdatePetName() throws Exception {
		Pet pet7 = this.pets.findById(7);
		String oldName = pet7.getName();

		String newName = oldName + "X";
		pet7.setName(newName);
		this.pets.save(pet7);

		pet7 = this.pets.findById(7);
		assertThat(pet7.getName()).isEqualTo(newName);
	}

	@Test
	void shouldFindVets() {
		Collection<Vet> vets = this.vets.findAll();

		Vet vet = EntityUtils.getById(vets, Vet.class, 3);
		assertThat(vet.getLastName()).isEqualTo("Douglas");
		assertThat(vet.getNrOfSpecialties()).isEqualTo(2);
		assertThat(vet.getSpecialties().get(0).getName()).isEqualTo("dentistry");
		assertThat(vet.getSpecialties().get(1).getName()).isEqualTo("surgery");
	}

	@Test
	@Transactional
	void shouldAddNewVisitForPet() {
		Pet pet7 = this.pets.findById(7);
		int found = pet7.getVisits().size();
		Visit visit = new Visit();
		pet7.addVisit(visit);
		visit.setDescription("test");
		this.visits.save(visit);
		this.pets.save(pet7);

		pet7 = this.pets.findById(7);
		assertThat(pet7.getVisits().size()).isEqualTo(found + 1);
		assertThat(visit.getId()).isNotNull();
	}

	@Test
	void shouldFindVisitsByPetId() throws Exception {
		Collection<Visit> visits = this.visits.findByPetId(7);
		assertThat(visits).hasSize(2);
		Visit[] visitArr = visits.toArray(new Visit[visits.size()]);
		assertThat(visitArr[0].getDate()).isNotNull();
		assertThat(visitArr[0].getPetId()).isEqualTo(7);
	}

}
